(function () {
    'use strict';

    describe('opentele.restApiServices.video service', function () {
        var httpBackend, httpProvider, location, videoService, appContext, timeout;

        var videoConference = {
            pendingConferenceUrl: "http://www.example.com/pollForPendingConference",
            pendingMeasurementUrl: "http://www.example.com/patientPendingMeasurement",
            measurementFromPatientUrl: "http://www.example.com/measurementFromPatient"
        };

        beforeEach(module('opentele.restApiServices.video'));
        beforeEach(module('opentele.restApiServices.interceptors'));

        afterEach(function () {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        beforeEach(inject(function ($http, $location, $timeout, _appContext_) {
            httpProvider = $http;
            location = $location;
            appContext = _appContext_;
            timeout = $timeout;
        }));

        beforeEach(inject(function (_videoService_, $httpBackend) {
            videoService = _videoService_;
            httpBackend = $httpBackend;
        }));

        describe('poll for pending conference', function () {

            it('should not change location when status is 401', function () {

                httpBackend.whenGET("http://www.example.com/pollForPendingConference").respond(401);

                videoService.pollForPendingConference(videoConference);

                httpBackend.flush(1);
                expect(location.path()).toBe("/login");
            });

            it('should set appContext and change location when status is 200', function () {

                httpBackend.whenGET("http://www.example.com/pollForPendingConference").respond({
                    roomKey: "237",
                    serviceUrl: "http://www.example.com/measurementFromPatient"
                });

                videoService.pollForPendingConference(videoConference);

                httpBackend.flush();
                expect(videoConference.roomKey).toEqual("237");
                expect(videoConference.serviceUrl)
                    .toEqual("http://www.example.com/measurementFromPatient");
                expect(location.path()).toBe("/joinConference");
            });

            it('should retry when request fails', function() {
              httpBackend.expectGET("http://www.example.com/pollForPendingConference").respond(504);
              httpBackend.whenGET("http://www.example.com/pollForPendingConference").respond({
                  roomKey: "237",
                  serviceUrl: "http://www.example.com/measurementFromPatient"
              });

              videoService.pollForPendingConference(videoConference);
              httpBackend.flush(1);
              expect(location.path()).not.toBe("/error");

              timeout.flush();
              httpBackend.flush();
              expect(location.path()).toBe("/joinConference");
            });

        });

        describe('cancel pending conference polling', function () {

            it('should cancel conference polling when requested', function () {
                var requestCancelled = true;
                httpBackend.whenGET(videoConference.pendingConferenceUrl).respond(function () {
                    requestCancelled = false;
                    return [400];
                });

                videoService.pollForPendingConference(videoConference);
                videoService.cancelPendingConferencePolling();

                httpBackend.verifyNoOutstandingRequest();
                expect(requestCancelled).toBeTruthy();
            });
        });

        describe('poll for pending measurement', function () {

            it('should pass result to onSuccess', function () {

                var pendingMeasurementUrl = "http://www.example.com/patientPendingMeasurement";

                httpBackend.whenGET(pendingMeasurementUrl).respond({
                    type: "SATURATION"
                });

                var passedResult;
                videoService.pollForPendingMeasurement(pendingMeasurementUrl,
                    function (result) {
                        passedResult = result;
                    });

                httpBackend.flush();
                expect(passedResult).toEqual({
                    type: "SATURATION"
                });
            });
        });

        describe('send measurement from patient', function () {

            it('should call onSuccess when successfull', function () {

                var measurementFromPatientUrl = "http://www.example.com/measurementFromPatient";

                httpBackend.whenPOST(measurementFromPatientUrl).respond(200);

                var onErrorCalled = false;
                var onSuccessCalled = false;
                var measurement = {};
                videoService.sendMeasurementFromPatient(measurementFromPatientUrl,
                    measurement,
                    function () {
                        onSuccessCalled = true;
                    },
                    function () {
                        onErrorCalled = true;
                    });

                httpBackend.flush();
                expect(onErrorCalled).toEqual(false);
                expect(onSuccessCalled).toEqual(true);
            });

            it('should call onError when failed', function () {

                var measurementFromPatientUrl = "http://www.example.com/measurementFromPatient";

                httpBackend.whenPOST(measurementFromPatientUrl).respond(401);

                var onErrorCalled = false;
                var onSuccessCalled = false;
                var measurement = {};
                videoService.sendMeasurementFromPatient(measurementFromPatientUrl,
                    measurement,
                    function () {
                        onSuccessCalled = true;
                    },
                    function () {
                        onErrorCalled = true;
                    });

                httpBackend.flush();
                expect(onErrorCalled).toEqual(true);
                expect(onSuccessCalled).toEqual(false);
            });

        });

    });
} ());
