(function() {
    'use strict';

    var categoryLinks = angular.module('opentele.controllers.categoryLinks', [
        'ngRoute',
        'opentele.restApiServices',
        'opentele-commons.nativeServices'
    ]);

    categoryLinks.config(function ($routeProvider) {
        $routeProvider.when('/category_links', {
            title: 'LINKS_TITLE',
            templateUrl: 'areas/categoryLinks/categoryLinks.html'
        });
    });

    categoryLinks.controller('CategoryLinksCtrl', function ($scope, $location, $window,
                                                            headerService, appContext,
                                                            linksCategories, nativeService) {

        $scope.$on('backClick', function() {
            $window.history.back();
        });

        $scope.showPopup = function(index) {
            $scope.model.showPopup = true;
            $scope.model.currentLink = $scope.model.categoryLinks[index].url;
        };

        $scope.hidePopup = function() {
            $scope.model.showPopup = false;
            $scope.model.currentLink = undefined;
        };

        $scope.showLink = function() {
            nativeService.openUrl($scope.model.currentLink);
            $scope.hidePopup();
        };

        headerService.setBack(true);
        if (!appContext.requestParams.containsKey('selectedLinksCategory')) {
            $location.path('/menu');
            return;
        }

        var selectedLinksCategory = appContext.requestParams.getAndClear('selectedLinksCategory');

        $scope.model = {};
        $scope.model.linksCategory = selectedLinksCategory;
        linksCategories.get(selectedLinksCategory, function(categoryLinks) {

            $scope.model.name = categoryLinks.name;
            $scope.model.categoryLinks = categoryLinks.categoryLinks;

        });
    });

}());
