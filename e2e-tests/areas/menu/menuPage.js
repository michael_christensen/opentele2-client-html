(function() {
    'use strict';

    var MenuPage = function() {
        this.menuList = element(by.id('menu-list'));
        this.menuItems = element.all(by.binding('menuItem.name'));

        this.toMeasurements = function() {
            element.all(by.id('menu-items')).
                get(0).
                $('a').
                click();
        };

        this.toMessages = function() {
            element.all(by.id('menu-items')).
                get(1).
                $('a').
                click();
        };

        this.toAcknowledgements = function() {
            element.all(by.id('menu-items')).
                get(2).
                $('a').
                click();
        };

        this.toMyMeasurements = function() {
            element.all(by.id('menu-items')).
                get(3).
                $('a').
                click();
        };

        this.toLinksCategories = function() {
            element.all(by.id('menu-items')).
                get(4).
                $('a').
                click();
        };

        this.toChangePassword = function() {
            element(by.css('a[href^="#/change_password"]')).click();
        };

        this.toMedicineList = function() {
            element(by.css('a[href^="#/medicine_list"]')).click();
        };
    };

    module.exports = new MenuPage();

}());
