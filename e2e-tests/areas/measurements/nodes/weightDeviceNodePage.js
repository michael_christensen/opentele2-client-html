(function() {
    'use strict';

    var WeightDeviceNodePage = function() {
        this.heading = element(by.binding('nodeModel.heading'));
        this.info = element(by.binding('nodeModel.info'));
        this.error = element(by.binding('nodeModel.error'));
        this.weight = element(by.model('nodeModel.weight'));
    };

    module.exports = new WeightDeviceNodePage();

}());
