(function() {
    'use strict';

    var TemperatureManualPage = function() {

        this.enterValue = function(value) {
            element(by.model('nodeModel.temperatureMeasurement')).sendKeys(value);
        };
    };

    module.exports = new TemperatureManualPage();
}());
