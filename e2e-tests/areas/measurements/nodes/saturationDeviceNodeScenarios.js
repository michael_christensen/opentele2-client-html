(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing saturation device node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var questionnairePage = require('../questionnairePage.js');
        var ioNodePage = require('./ioNodePage.js');
        var saturationDeviceNodePage = require('./saturationDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
            measurementsPage.toQuestionnaire("Saturation", "0.1");
        });

        it('should show questionnaire', function () {
            ioNodePage.headings.then(function(items) {
                expect(items.length).toBe(1);
                expect(items[0].getText()).toMatch(/Saturation/);
            });
        });

        it('should navigate to saturation device node', function () {
            questionnairePage.clickCenterButton();
            expect(saturationDeviceNodePage.heading.getText()).toMatch(/Saturation/);
        });

        it('should see all info texts and fill out form', function(done) {
            questionnairePage.clickCenterButton();
            expect(saturationDeviceNodePage.heading.getText()).toMatch(/Saturation/);

            var infoDeferred = protractor.promise.defer();
            var pulseDeferred = protractor.promise.defer();
            var saturationDeferred = protractor.promise.defer();
            setTimeout(function() {
                saturationDeviceNodePage.info.getText()
                    .then(function(text) {
                        infoDeferred.fulfill(text);
                    });
                saturationDeviceNodePage.saturation.getAttribute('value')
                    .then(function(value) {
                        saturationDeferred.fulfill(value);
                    });
                saturationDeviceNodePage.pulse.getAttribute('value')
                    .then(function(value) {
                        pulseDeferred.fulfill(value);
                        done();
                });
            }, 4500);
            expect(pulseDeferred).toEqual('80');
            expect(saturationDeferred).toEqual('98');
            expect(infoDeferred).toMatch(/Waiting for measurement/);

        });
    });

}());
