(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing blood pressure devide node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var questionnairePage = require('../questionnairePage.js');
        var ioNodePage = require('./ioNodePage.js');
        var bloodPressureDeviceNodePage = require('./bloodPressureDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
            measurementsPage.toQuestionnaire("Blodtryk og puls", "1.0");
        });

        it('should show questionnaire', function () {
            ioNodePage.headings.then(function(items) {
                expect(items.length).toBe(2);
                expect(items[0].getText()).toMatch(/Blodtryk/);
                expect(items[1].getText()).toMatch(/manchetten på armen/);
            });
        });

        it('should navigate to blood pressure device node', function () {
            questionnairePage.clickCenterButton();
            expect(bloodPressureDeviceNodePage.heading.getText()).toMatch(/Blodtryk/);
        });

        it('should see all info texts and fill out form', function(done) {
            questionnairePage.clickCenterButton();
            expect(bloodPressureDeviceNodePage.heading.getText()).toMatch(/Blodtryk/);

            var infoDeferred = protractor.promise.defer();
            var pulseDeferred = protractor.promise.defer();
            var systolicDeferred = protractor.promise.defer();
            var diastolicDeferred = protractor.promise.defer();
            setTimeout(function() {
                bloodPressureDeviceNodePage.info.getText()
                    .then(function(text) {
                        infoDeferred.fulfill(text);
                    });
                bloodPressureDeviceNodePage.systolic.getAttribute('value')
                    .then(function(value) {
                        systolicDeferred.fulfill(value);
                    });
                bloodPressureDeviceNodePage.diastolic.getAttribute('value')
                    .then(function(value) {
                        diastolicDeferred.fulfill(value);
                    });
                bloodPressureDeviceNodePage.pulse.getAttribute('value')
                    .then(function(value) {
                        pulseDeferred.fulfill(value);
                        done();
                    });
            }, 4500);
            expect(pulseDeferred).toEqual('80');
            expect(systolicDeferred).toEqual('122');
            expect(diastolicDeferred).toEqual('95');
            expect(infoDeferred).toMatch(/Connected/);

        });
    });

}());
