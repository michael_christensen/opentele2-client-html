(function () {
    'use strict';

    describe('perform measurements', function () {
        var loginPage = require("../login/loginPage.js");
        var menuPage = require("../menu/menuPage.js");
        var performMeasurementsPage = require("../measurements/performMeasurementsPage.js");
        var helper = require("../scenarioHelper.js");

        describe('when authenticated', function () {

            beforeEach(function () {
                loginPage.get();
                loginPage.doLogin('nancyann', 'abcd1234');

                menuPage.toMeasurements();
            });

            it('should navigate to measurements page', function () {
                expect(performMeasurementsPage.questionnaireList).toBeDefined();
            });

            it('should show list of questionnaires', function () {
                performMeasurementsPage.questionnaireNames.then(function (names) {
                    expect(names.length).toEqual(35);
                    expect(names[0].getText()).toMatch(/Blodsukker/i);
                    expect(names[2].getText()).toMatch(/Hæmoglobin/i);
                });
            });

            it('should show overdue questionnaires as marked', function () {
                helper.waitFor(performMeasurementsPage.questionnaireNames, function (names, deferred) {
                    helper.getCssClasses(names[0]).then(function(classes) {
                        deferred.fulfill(helper.containsCssClass(classes, 'marked-button'));
                    });
                });

                performMeasurementsPage.questionnaireNames.then(function (names) {
                    expect(helper.getCssClasses(names[0])).toMatch(/marked-button/i);
                    expect(helper.getCssClasses(names[4])).not.toMatch(/marked-button/i);
                    expect(helper.getCssClasses(names[6])).not.toMatch(/marked-button/i);
                });
            });

            it('should redirect to questionnaire page when questionnaire clicked', function () {
                performMeasurementsPage.toQuestionnaire("Blodsukker (manuel)", "0.1");
                expect(browser.getLocationAbsUrl()).toMatch(/\/questionnaire/);
            });
        });

        describe('patient with only one questionnaire assigned', function () {
            beforeEach(function () {
                loginPage.get();
                loginPage.doLogin('rene', '1234');
            });

            it('should automatically redirect to questionnaire', function () {
                menuPage.toMeasurements();
                expect(browser.getLocationAbsUrl()).toMatch(/\/questionnaire/);
            });

        });

        describe('when not authenticated', function () {
            beforeEach(function () {
                loginPage.get();
                loginPage.doLogin('measurements401', '1234');
            });

            it('should redirect to login page', function () {
                menuPage.toMeasurements();
                expect(browser.getLocationAbsUrl()).toMatch("/login");
            });
        });
    });
} ());
